# WWCAROUSEL
A jQuery infinite carousel plugin.

##Usage
* Create a viewport div with specific dimensions of what you want shown.
* Create elements for next and previous controls and style them.
* Put all elements in a ul or ol
* Invoke with:
$(VIEWPORT DIV SELECTOR).wwcarousel({
  container: $(UL OR OL SELECTOR),
  next: $(SELECTOR FOR NEXT CONTROL),
  prev: $(SELECTOR FOR PREVIOUS CONTROL)
});

##Optional Options
* autoSpeed: (time in milliseconds) - Make the carousel advance
  automatically
