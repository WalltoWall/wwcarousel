(function($){
  $.fn.wwcarousel = function(options){
    var defaults = {
      afterAnimate: function(){},
      speed: 400
    };

    var viewport = $(this);
    options = $.extend(defaults, options);

    this.initialize = function(){
      options.elements = $(options.container).find('li');
      options.container.initialLeft = $(options.container).position().left;
      options.width = $(options.elements).first().outerWidth(true);
      options.count = $(options.elements).length;
      if(options.autoSpeed){
        setTimeout(function(){autoAdvance();}, options.autoSpeed);
      }
      $(viewport).css('overflow', 'hidden');
      $(options.container).css('width', (options.width*options.count)+"px");
      activateControls();
    };

    var activateControls = function(){
      $(options.next).click(function(){nextClicked();});
      $(options.prev).click(function(){prevClicked();});
    };

    var nextClicked = function(){
      unbindControls();
      $(options.container).stop(true,true).animate({left: options.container.initialLeft-(options.width)}, options.speed, function(){
        $(options.container).find('li').first().appendTo($(options.container));
        $(options.container).css("left", (options.container.initialLeft)+'px');
        activateControls();
      });
    };

    var prevClicked = function(){
      unbindControls();
      $(options.container).css("left", (-options.width)+"px");
      $(options.container).find('li').last().prependTo($(options.container));
      $(options.container).stop(true, true).animate({left: options.container.initialLeft}, options.speed, function(){activateControls();});
    };

    var unbindControls = function(){
      $(options.next).unbind('click');
      $(options.prev).unbind('click');
    };

    var autoAdvance = function(){
      nextClicked();
      setTimeout(function(){autoAdvance();}, options.autoSpeed);
    };

    return this.initialize();
  };
})(jQuery);
